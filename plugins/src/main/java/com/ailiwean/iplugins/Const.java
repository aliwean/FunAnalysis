package com.ailiwean.iplugins;

public class Const {

    //extension配置名
    public static String configName = "funAnalysis";
    //transform任务名称
    public static String transformName = "funAnalysisTask";
    //默认tag
    public static String defaultTag = "funAnalysis";
    //注解签名
    public static String annotationDesc = "Lcom/ailiwean/annotation/Analysis";

}
